﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
public class CameraFollow : MonoBehaviour
{
    CatchBall cB;
    public Transform target;
    public float smoothspeed;
    public Vector3 offset;
    private float ballCountOffset;
    
    Vector3 supportvec;
    private void Awake()
    {
        cB = GameObject.FindWithTag("Player").GetComponent<CatchBall>();
    }

    private void Update()
    {
        ballCountOffset = cB.myList.Count / 15f;
    }
    void FixedUpdate()
    {
        supportvec = new Vector3(target.position.x, target.position.y, 0);
        Vector3 desiredPosition = (target.position - supportvec) + offset * (1 + ballCountOffset);
        Vector3 smoothedPosition = Vector3.Lerp(transform.position, desiredPosition, smoothspeed * Time.deltaTime);
        transform.position = smoothedPosition;
    }
}