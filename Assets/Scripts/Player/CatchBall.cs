﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CatchBall : MonoBehaviour
{
    //RollingBall rollingBall;
    CapsuleCollider m_Collider;

    public Material red;
    public Material orange;
    public Material green;
    public SkinnedMeshRenderer myRenderer;

    public bool playerRed, playerOrange, playerGreen;
    public int ballCount;

    public List<int> myList = new List<int>();
    public int number;

    private void Start()
    {
        playerRed = true;
        m_Collider = GetComponent<CapsuleCollider>();
    }

    private void Update()
    {
        SetCollider();
    }


    void SetCollider()
    {
        m_Collider.height = 1.4f * myList.Count + 1f;
    }

    private void OnTriggerEnter(Collider collider)
    {

        if (collider.gameObject.CompareTag("Ball_Red"))
        {
            playerRed = true;
            collider.GetComponent<Animator>().enabled = true;
        }

        if (playerRed)
        {
            if (collider.gameObject.CompareTag("Ball_Orange"))
            {
                //Debug.Log("Hit");
                
                    //rollingBall.testOrange = true;

            }
        }
    }
}
