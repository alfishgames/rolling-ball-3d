using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour
{
    CatchBall catchBall;

    public float horizontalSpeed;
    public float forwardSpeed;
    public float lerpRatio;


    private float lerpValue;
    private Vector3 moveVector;
    private Vector3 playerOffset;
    private Rigidbody rb;

    void Start()
    {
        rb = GetComponent<Rigidbody>();
        catchBall = GameObject.FindWithTag("Player").GetComponent<CatchBall>();
    }

    private void Update()
    {
        
    }

    #region LifeTime
    void FixedUpdate()
    {
        lerpValue += Time.deltaTime / lerpRatio;
        playerOffset = Vector3.up * Mathf.Lerp(0f, catchBall.myList.Count * .5f,lerpValue);

        moveVector.z = transform.position.z + (forwardSpeed * Time.fixedDeltaTime);
        if (Input.GetMouseButton(0))
        {
            float mouseX = Input.GetAxis("Mouse X");
            Vector3 myPos = transform.localEulerAngles;

            moveVector.x = transform.position.x + mouseX * horizontalSpeed * Time.fixedDeltaTime;

            Quaternion deltaRotation = Quaternion.Euler(new Vector3(0, mouseX * 60,0));
            transform.rotation = Quaternion.Slerp(transform.rotation, deltaRotation, Time.deltaTime * 10);

        }
        else
        {
            transform.rotation = Quaternion.Slerp(transform.rotation, new Quaternion(transform.rotation.x,0,transform.rotation.z,transform.rotation.w), Time.deltaTime * horizontalSpeed);
        }
        rb.MovePosition(Vector3.Lerp(moveVector, moveVector + playerOffset, lerpValue));
        rb.MovePosition(Vector3.Lerp(moveVector + playerOffset, moveVector + playerOffset, lerpValue));
    }
    #endregion

}
