﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RollingBall : MonoBehaviour
{
    CatchBall cB;
    

    private Rigidbody myRb;
    private Collider myCol;
    public Collider myTriggerCol;
    private Collider touched;

    public Transform player;
    public Collider playerCol;


    public int myNumber;
    public float timer;
    public float lerpRatio;
    public float lerpRatioY;
    public float k;
    float rollingSpeed = 300f;
    public bool amIRed, amIOrange, amIGreen;
    public bool followPlayer;

    private float lerpValue;
    private float lerpValueY;
    private float posX;
    private float posY;

    public int abc;

    Vector3 crashForce = new Vector3(2f, 1f, -1f);
    Vector3 myCrashForce = new Vector3(-2f, 1f, -1f);


    private void Awake()
    {
        playerCol = GameObject.FindWithTag("Player").GetComponent<Collider>();
        myRb = GetComponent<Rigidbody>();
        myCol = GetComponent<Collider>();
        cB = GameObject.FindWithTag("Player").GetComponent<CatchBall>();
    }


    private void FixedUpdate()
    {
        MovementController();

        if (followPlayer)
        {
            FollowPlayer();
            rollingSpeed = 0f;
        }
    }

    private void Update()
    {
        Physics.IgnoreCollision(myTriggerCol, playerCol);
    }

    void MovementController()
    {
        myRb.AddForce(Vector3.back * rollingSpeed * Time.deltaTime);
    }

    void FollowPlayer()
    {
        myRb.velocity = Vector3.zero;

        posY = player.position.y * transform.localScale.y * abc * Vector3.down.y * 0.5f;
        posX = Mathf.Lerp(transform.position.x, player.transform.position.x, lerpValueY);
        
        lerpValueY += Time.deltaTime / lerpRatioY;

        Vector3 desiredPosition = new Vector3(posX, posY, player.position.z);

        if (abc == 1)
        {
            lerpValue += Time.deltaTime / lerpRatio;
            // transform.position = Vector3.Lerp(transform.position, desiredPosition, lerpValue);
            transform.position = Vector3.Lerp(transform.position, player.position + Vector3.down * transform.localScale.z * .5f * abc, lerpValue);
        }
        else
        {
            lerpValue += Time.deltaTime / lerpRatio;
            //transform.position = Vector3.Lerp(transform.position, desiredPosition, lerpValue);
            k = .78f ;
            if (abc >=3) k = .85f + .01f * abc;
            transform.position = Vector3.Lerp(transform.position, player.position + Vector3.down * transform.localScale.z * 1f * abc * k, lerpValue);
        }
    }


    private void OnTriggerEnter(Collider other)
    {
        if (amIRed)
        {
            if (other.gameObject.CompareTag("Player"))
            {
                cB.myList.Add(cB.ballCount++);
                Debug.Log("Player Colliding Ball!");
                Physics.IgnoreCollision(myCol, playerCol);
                abc = cB.myList.Count;
                followPlayer = true;
                Debug.Log(abc);        
            }

            if (other.gameObject.CompareTag("Ball_Orange"))
            {
                followPlayer = false;
                Debug.Log("Dont Follow player!");
                SphereCollider[] colliders = other.GetComponents<SphereCollider>();
                for (int i = 0; i < colliders.Length; i++)
                {
                    colliders[i].enabled = false;
                }
                other.GetComponent<SphereCollider>().enabled = false;
                cB.myList.RemoveAt(cB.myList.Count - 1);
                myRb.velocity = myCrashForce * 2f;
                other.GetComponent<Rigidbody>().AddExplosionForce(45f, Vector3.up, 150f);////
                other.GetComponent<Rigidbody>().velocity = crashForce * 3f;
                myRb.AddExplosionForce(15f, Vector3.up, 150f);
                Debug.Log(cB.myList.Count);
            }
            
            if (other.gameObject.CompareTag("Ball_Green"))
            {
                followPlayer = false;
                Debug.Log("Dont Follow player!");
                SphereCollider[] colliders = other.GetComponents<SphereCollider>();
                for (int i = 0; i < colliders.Length; i++)
                {
                    colliders[i].enabled = false;
                }
                other.GetComponent<SphereCollider>().enabled = false;
                cB.myList.RemoveAt(cB.myList.Count - 1);
                myRb.velocity = myCrashForce * 2f;
                other.GetComponent<Rigidbody>().AddExplosionForce(45f, Vector3.up, 150f);/////
                other.GetComponent<Rigidbody>().velocity = crashForce * 3f;
                myRb.AddExplosionForce(15f, Vector3.up, 15f);
                Debug.Log(cB.myList.Count);
            }
        }
 
    }
}
